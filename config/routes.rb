Rails.application.routes.draw do

  resources :bords, only: [:create, :show], param: :uniq_link do
    resources :lists, only: [:new, :create, :destroy] do
      resources :cards, only: [:new, :create, :destroy, :edit, :update] do
        put '/tie', to: 'cards#tie'
      end
    end
    get '/short', to: 'bords#short_show'
  end

  root to: 'root#index'
end
