module ServiceBlk
  module EventHelpers

    private
    def send_card_on_created(bord, card)
      BordsChannel.broadcast_to(bord, {type: :card_created, data: {list_id: card.list_id}})
    end

    def send_card_updated(bord, card)
      BordsChannel.broadcast_to(bord, {type: :card_updated, data: {list_id: card.list_id}})
    end

    def send_card_tie(old_list, new_list)
      BordsChannel.broadcast_to(new_list.bord, {type: :card_tie, data: {list_id_arr: [old_list.id, new_list.id]}})
    end

    def send_card_destroy(bord, card)
      BordsChannel.broadcast_to(bord, {type: :card_destroy, data: {list_id: card.list_id}})
    end

    def send_list_created(bord, list)
      BordsChannel.broadcast_to(bord, {type: :list_created, data: {list_id: list.id}})
    end

    def send_list_destroy(bord, list)
      BordsChannel.broadcast_to(bord, {type: :list_destroy, data: {list_id: list.id}})
    end
  end
end