module ContrBlock
  module BordHelpers

    private
    def get_bord_by_key(key, includes_keys_arr = [])
      @bord = (unless includes_keys_arr.blank?
                 Bord.includes(*includes_keys_arr)
      else
        Bord
      end).find_by!(uniq_link: params[key])
    end

    def set_bord_to_enc_cookies
      cookies.encrypted[:uniq_link] = @bord.uniq_link
    end
  end
end