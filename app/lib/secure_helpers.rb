require 'securerandom'

module SecureHelpers
  def self.gen_rnd_str(len)
    SecureRandom.alphanumeric(len)
  end
end