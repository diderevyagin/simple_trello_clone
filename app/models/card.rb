class Card < ApplicationRecord

  belongs_to :list, validate: false
  validates_presence_of :list, :title


end