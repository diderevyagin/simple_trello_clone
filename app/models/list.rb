class List < ApplicationRecord

  belongs_to :bord, validate: false
  validates_presence_of :bord, :title

  has_many :cards, dependent: :destroy, validate: false

end