class Bord < ApplicationRecord

  validates_presence_of :title, :uniq_link
  validates_uniqueness_of :uniq_link

  has_many :lists, dependent: :restrict_with_exception, validate: false

  def to_param
    uniq_link
  end
end