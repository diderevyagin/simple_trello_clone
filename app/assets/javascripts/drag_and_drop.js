const eventForReloadPage = Object.freeze(new Set(['card_created', 'card_updated', 'card_tie', 'card_destroy', 'list_created', 'list_destroy']));

$(function() {
    prepareForDragAndDrop();
});

function tryTieCard(dataForCall) {
    Rails.ajax({
        type: "PUT",
        url: "/bords/"+ dataForCall.bordUniqLink +"/lists/"+ dataForCall.dstListId +"/cards/"+ dataForCall.cardId +"/tie",
        data: {},
        success: function(res){
                setTimeout(() => {
                    reloadBordPage();
                }, 30);
            },
        error: function(res){
        }
    });
}

function getBordUniqLink() {
    return $("#bord_uniq_link").attr("uniq_link");
}

function reloadBordPage() {
    let bordUniqLink = getBordUniqLink();
    if (bordUniqLink == null) { return; }

    Rails.ajax({
        type: "GET",
        url: "/bords/"+ bordUniqLink +"/short",
        cache: false,
        success: function(res){
            $("#bord_table_data").html(res.body.innerHTML);
            prepareForDragAndDrop();
        },
        error: function(res){
        }
    });
}

function prepareForDragAndDrop() {
    $("div.draggable").draggable({
        revert: true,
    });

    $("div.column").droppable({
        drop: function(_, ui) {
            let destEl = $(this);
            let sourceEl = ui.draggable;

            let srcListId = sourceEl.attr('list_id');
            let dstListId = destEl.attr('list_id');

            if ((srcListId == null) || (dstListId == null)) { return; }

            if (srcListId == dstListId) { return; }

            let cardId = sourceEl.attr('card_id');
            if (cardId == null) { return; }

            let bordUniqLink = getBordUniqLink();
            if (bordUniqLink == null) { return; }

            tryTieCard({
                cardId: cardId,
                bordUniqLink: bordUniqLink,
                dstListId: dstListId
            });
        }
    });
}
