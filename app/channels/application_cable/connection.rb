module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :cur_bord

    def connect
      self.cur_bord = find_bord
    end

    private
    def find_bord
      if (bord = Bord.find_by(uniq_link: cookies.encrypted[:uniq_link]))
        bord
      else
        reject_unauthorized_connection
      end
    end
  end
end
