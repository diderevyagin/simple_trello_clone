module CardMod
  class DestroyService
    include ServiceBlk::EventHelpers

    def initialize(bord, card)
      @bord, @card = bord, card
    end

    def call
      send_card_destroy(@bord, @card) if @card.destroy
    end
  end
end