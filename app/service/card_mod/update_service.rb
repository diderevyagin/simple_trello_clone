module CardMod
  class UpdateService
    include ServiceBlk::EventHelpers

    def initialize(bord, card)
      @bord, @card = bord, card
    end

    def call(params)
      @card.update(params).tap do |res|
        send_card_updated(@bord, @card) if res
      end
    end
  end
end