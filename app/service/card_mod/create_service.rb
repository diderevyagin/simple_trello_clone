module CardMod
  class CreateService
    include ServiceBlk::EventHelpers

    def initialize(bord, list)
      @bord, @list = bord, list
    end

    def call(params)
      Card.create({list: @list}.merge(params)).tap do |c|
        send_card_on_created(@bord, c) if c.persisted?
      end
    end

  end
end
