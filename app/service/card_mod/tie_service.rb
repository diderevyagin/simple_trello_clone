module CardMod
  class TieService
    include ServiceBlk::EventHelpers

    def initialize(list, card)
      @list, @card = list, card
    end

    def call
      @old_list = @card.list
      @card.list = @list
      @card.save.tap {|res| send_card_tie(@old_list, @list) if res }
    end
  end
end