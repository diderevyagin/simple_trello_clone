module ListMod
  class DestroyService
    include ServiceBlk::EventHelpers

    def initialize(bord, list)
      @bord, @list = bord, list
    end

    def call
      send_list_destroy(@bord, @list) if @list.destroy
    end
  end
end
