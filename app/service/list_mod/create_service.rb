module ListMod
  class CreateService
    include ServiceBlk::EventHelpers

    def initialize(bord)
      @bord = bord
    end

    def call(params)
      List.create({bord: @bord}.merge(params)).tap do |l|
        send_list_created(@bord, l) if l.persisted?
      end
    end
  end
end
