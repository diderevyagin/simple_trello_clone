class BordCreateService
  SHORT_LINK_LEN = 10
  UNIQ_LINK_KEY = :uniq_link

  def initialize(bord_params)
    @bord_params = bord_params
  end

  def call
    prep_data_for_create
    try_create!
    @bord.tap { analize_necessity_recall }
  rescue ActiveRecord::RecordNotUnique => e
    call
  end

  private
  def prep_data_for_create
    @bord_params[UNIQ_LINK_KEY] = SecureHelpers::gen_rnd_str(SHORT_LINK_LEN)
  end

  def try_create!
    (@bord = Bord.new(@bord_params)).tap {|e| e.save }
  end

  def analize_necessity_recall
    call if @bord.errors.keys.include?(UNIQ_LINK_KEY)
  end
end