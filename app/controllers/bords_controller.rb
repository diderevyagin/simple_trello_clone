class BordsController < ApplicationController
  include ContrBlock::BordHelpers
  before_action(only: [:show]) { get_bord_by_key(:uniq_link, [:lists, lists: [:cards]]) }
  before_action(only: [:short_show]) { get_bord_by_key(:bord_uniq_link, [:lists, lists: [:cards]]) }
  before_action :set_bord_to_enc_cookies, only: [:show]

  layout false, only: [:short_show]

  def create
    @bord = BordCreateService.new(bord_params).()

    if @bord.persisted?
      redirect_to bord_path(uniq_link: @bord.uniq_link)
    else
      render 'bords/new'
    end
  end

  def show
  end

  def short_show
    render partial: 'bords/table_data'
  end

  private
  def bord_params
    params.require(:bord).permit(:title)
  end
end
