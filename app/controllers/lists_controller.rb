class ListsController < ApplicationController
  include ContrBlock::BordHelpers
  before_action { get_bord_by_key(:bord_uniq_link) }
  before_action :get_list, only: [:destroy]

  def new
    @list = @bord.lists.new
  end

  def create
    @list = ListMod::CreateService.new(@bord).(list_params)
    if @list.persisted?
      redirect_to bord_path(uniq_link: @bord.uniq_link), notice: I18n.t("controllers.lists.create.successful_msg")
    else
      render 'lists/new'
    end
  end

  def destroy
    ListMod::DestroyService.new(@bord, @list).()
    redirect_to bord_path(uniq_link: @bord.uniq_link), notice: I18n.t("controllers.lists.destroy.successful_msg")
  end

  private
  def list_params
    params.require(:list).permit(:title)
  end

  def get_list
    @list = @bord.lists.where(id: params[:id]).first
    if @list.blank?
      redirect_to bord_path(uniq_link: @bord.uniq_link), alert: I18n.t("controllers.lists.not_found")
    end
  end
end
