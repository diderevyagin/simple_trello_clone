class CardsController < ApplicationController
  include ContrBlock::BordHelpers

  before_action { get_bord_by_key(:bord_uniq_link) }
  before_action :get_list
  before_action :get_card, only: [:edit, :update, :destroy]
  before_action(only: [:tie]) { get_card_with_check_bord }

  def create
    @card = CardMod::CreateService.new(@bord, @list).(card_params)
    if @card.persisted?
      redirect_to bord_path(uniq_link: @bord.uniq_link), notice: I18n.t("controllers.cards.create.successful_msg")
    else
      render 'cards/new'
    end
  end

  def edit
    render 'cards/edit'
  end

  def update
    if CardMod::UpdateService.new(@bord, @card).(card_params)
      redirect_to bord_path(uniq_link: @bord.uniq_link), notice: I18n.t("controllers.cards.update.successful_msg")
    else
      render 'cards/edit'
    end
  end

  def new
    @card = @list.cards.new
  end

  def destroy
    CardMod::DestroyService.new(@bord, @card).call
    redirect_to bord_path(uniq_link: @bord.uniq_link), notice: I18n.t("controllers.cards.destroy.successful_msg")
  end

  def tie
    head((CardMod::TieService.new(@list, @card).()) ? :ok : :bad_request)
  end

  private
  def get_list
    @list = @bord.lists.find_by!(id: params[:list_id])
  end

  def get_card
    @card = @list.cards.where(id: params[:id]).first
    if @card.blank?
      redirect_to bord_path(uniq_link: @bord.uniq_link), alert: I18n.t("controllers.cards.not_found")
    end
  end

  def get_card_with_check_bord
    @card = Card.joins(:list).where(lists: { bord: @bord }).find(params[:card_id])
  end

  def card_params
    params.require(:card).permit(:title)
  end
end
