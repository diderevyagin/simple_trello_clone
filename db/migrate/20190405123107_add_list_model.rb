class AddListModel < ActiveRecord::Migration[5.2]
  def change
    create_table :lists do |t|
      t.string :title, null: false
      t.references :bord, index: true, null: false
      t.timestamps
    end
  end
end
