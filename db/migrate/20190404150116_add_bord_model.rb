class AddBordModel < ActiveRecord::Migration[5.2]
  MAX_LINK_LEN = 20
  def change
    create_table :bords do |t|
      t.string :title, null: false
      t.string :uniq_link, null: false, limit: MAX_LINK_LEN, index: {unique: true}
      t.timestamps
    end
  end
end
