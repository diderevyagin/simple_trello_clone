class AddCardModel < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.string :title, null: false
      t.references :list, index: true, null: false
      t.timestamps
    end
  end
end
