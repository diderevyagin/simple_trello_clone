# README

* Ruby version: 2.6.2

## Install instructions
* step 1:
```shell
docker-compose build
```

* step 2:
```shell
docker-compose run --rm stc_app bundle install
```

* step 3, create database:
```shell
docker-compose run --rm stc_app bundle exec rails db:setup
```

## Run instructions

* step 1, migrate database:
```shell
docker-compose run --rm stc_app bundle exec rails db:migrate
```

* step 2, run:
```shell
docker-compose up
```

## View application
* You can view application by:
[Application url](http://127.0.0.1:8888/)

